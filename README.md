# GisTweet

## Instalación
1. Instalar PostgreSQL https://www.postgresql.org
2. Instalar PostGis http://postgis.net
3. Activar PostgresSQL server.
5. Importar Base de Datos a PostgreSQL 'GisTweet.pgsql'
5. Instalar pip https://pip.pypa.io/en/stable/installing/
6. Instalar requisitos

 `$ pip install -r requirements.txt`

7. Instalar paquetes para NLTK

``` [python]
>>> import nltk
>>> nltk.download()
```

## Configuración
1. En `PATH_TO/GisTweetWeb/config.py` configurar SQLALCHEMY_DATABASE_URI. más información en http://docs.sqlalchemy.org/en/latest/core/engines.html#postgresql

## Ejecución
* Activar PostgresSQL server.
* Entrar a `PATH_TO/GisTweetWeb` y ejecutar `python main.py`
* Entrar al navegador web http://localhost:5000
* Usar GisTweet

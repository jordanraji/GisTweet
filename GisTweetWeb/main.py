# -*- coding: utf-8 -*-
from flask import Flask, render_template, url_for, redirect
from flask import request, make_response
from flask import session
from flask import flash
from flask_wtf.csrf import CSRFProtect

from config import DevelopmentConfig

from sqlalchemy.sql import table, column, select, update, insert
from sqlalchemy import or_, and_
from sqlalchemy import text

from babel import *
from models import db
import forms
import nltk
import re
from collections import Counter
from string import punctuation

app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
csrf = CSRFProtect()

@app.route('/', methods = ['GET','POST'])
def index():
    form = forms.GisForm()
    rtv = []
    my_text = u''
    mostCommon = []
    if request.method == 'POST':
        lat = form.latitude.data
        lng = form.longitude.data
        rds = form.radio.data
        sql = text("select place, tweet_id, tweet_text, country, username from tweets where ST_DistanceSphere(geom, ST_MakePoint(" + lng + ","+ lat +")) <= "+ rds +" * 1609.344 * 0.62137;")
        result = db.engine.execute(sql)
        my_text = u''
        for row in result:
            my_text += row[2] + u" "
            rtv.append([row[0],row[1],row[2],row[3],row[4]])

        my_text = my_text.lower()

        words = re.findall(r'\w+', my_text,flags = re.UNICODE | re.LOCALE)

        stopwords_spanish = nltk.corpus.stopwords.words('spanish')
        stopwords_english = nltk.corpus.stopwords.words('english')
        stopwords_web = [u'http', u'https', u'@' , u'#', u':', u'/', u'co', u'n']
        stopwords = stopwords_spanish + stopwords_english + stopwords_web

        important_words=[]
        for word in words:
            if word not in stopwords:
                important_words.append(word)

        mostCommon = Counter(important_words).most_common(10)
        # print mC
    return render_template('base.html', form = form, rtv = rtv, mostCommon = mostCommon)

if __name__ == '__main__':
    csrf.init_app(app)
    db.init_app(app)
    with app.app_context():
        db.create_all()
    app.run(host='127.0.0.1', port=5000, threaded=True)

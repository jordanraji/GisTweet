# -*- coding: utf-8 -*-
import json
import datetime
import psycopg2

with open('tweets.json') as json_data:
    json_tweets = json.load(json_data)

# print d[0]
NUM_TWEETS =  len(json_tweets)

conn_string = "host='localhost' dbname='GisTweet' user='raji' password='2400'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor()


idx = 0;
while idx < NUM_TWEETS:
    # print json_tweets[idx]["coordinates"]
    query = 'INSERT INTO tweets (id, tweet_id, tweet_text, created_at, username, user_id, place, country, geom) VALUES ('\
        + unicode(idx) + ", " \
        + unicode(json_tweets[idx]["id"]) + ", " \
        + "'" + unicode(json_tweets[idx]["text"]) + "', " \
        + "'" + unicode(json_tweets[idx]["created_at"]) + "', " \
        + "'" + unicode(json_tweets[idx]["username"]) + "', " \
        + unicode(json_tweets[idx]["user_id"]) + ", " \
        + "'" + unicode(json_tweets[idx]["place"]) + "', " \
        + "'" + unicode(json_tweets[idx]["country"]) + "', " \
        + "ST_GeomFromText('POINT(" + unicode(json_tweets[idx]["coordinates"][0]) + " " + unicode(json_tweets[idx]["coordinates"][1]) + ")')" \
        +');'
    cursor.execute(query)
    idx += 1

conn.commit()

# CREATE TABLE tweets (
# id integer NOT NULL,
# tweet_id bigint,
# tweet_text text,
# created_at timestamp,
# username character(100),
# user_id bigint,
# place character(100),
# country character(100),
# geom geometry,
# CONSTRAINT pk_id PRIMARY KEY (id)
# );


# INSERT INTO tbl_properties (id, tweet_id, tweet_text, created_at, username,
#    user_id, place, country, geom) VALUES (1, 'London', 'N7 6PA', 'Holloway Road', 32, ST_
#    GeomFromText('POINT(-0.116190 51.556173)'));

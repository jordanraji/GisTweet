# -*- coding: utf-8 -*-
import tweepy
import json
import datetime
import codecs
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
# COORDENADAS TIENEN EL ASPECTO [LONGITUD, LATITUD]

CONSUMER_KEY = 'XXXX'
CONSUMER_SECRET = 'XXXX'
ACCESS_TOKEN = 'XXXX'
ACCESS_TOKEN_SECRET = 'XXXX'

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

api = tweepy.API(auth)

places = api.geo_search(query="BOL", granularity="country")
place_id = places[0].id

max_tweets = 1000
tweets = tweepy.Cursor(api.search, q="place:%s" % place_id).items(max_tweets)

for tweet in tweets:
    if tweet.coordinates != None:
        with codecs.open('tweets.json', encoding='utf-8') as f:
            data = json.load(f)
        # data.update(a_dict)
        entry = {
            "id": tweet.id,
            "user_id": tweet.user.id,
            "username": tweet.user.screen_name,
            "text": unicode(tweet.text),
            "created_at": unicode(tweet.created_at),
            "coordinates": [tweet.coordinates['coordinates'][0], tweet.coordinates['coordinates'][1]],
            "country": tweet.place.country,
            "place": unicode(tweet.place.name)
            }
        data.append(entry)
        with codecs.open('tweets.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, indent=4, ensure_ascii=False)